import AppRouter from "./routes/Routes";
import Navigation from "./components/navigate/Navigation";
import { BrowserRouter as Router } from "react-router-dom";
import { ToastProvider } from "react-toast-notifications";

function App() {
  return (
    <ToastProvider>
      <Router>
        <Navigation />
        <AppRouter />
      </Router>
    </ToastProvider>
  );
}

export default App;

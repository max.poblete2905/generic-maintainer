import axios from "axios";

const authAxios = axios.create({
  baseURL: "http://localhost:4000/api/",
});

const getToken = () => {
  return localStorage.getItem("token");
};
authAxios.interceptors.request.use((config) => {
  const token = getToken();
  config.headers.Authorization = `Bearer ${token}`;
  return config;
});

export default authAxios;

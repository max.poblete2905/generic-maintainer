import authAxios from "../axios/auth-axios";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { User } from "../models";

type EntityApiRestConfig = {
  entity: string;
  apiRoute: string;
};
const entityApiRest = <T>(config: EntityApiRestConfig) => {
  return {
    list: createAsyncThunk<T[]>(`${config.entity}/list`, async () => {
      try {
        const response = await authAxios.get(config.apiRoute);
        return response.data;
      } catch (e) {
        console.error(e);
        return [];
      }
    }),
    create: createAsyncThunk<T, User>(
      `${config.entity}/create`,
      async (data: User) => {
        try {
          const response = await authAxios.post(config.apiRoute, data);
          return response.data;
        } catch (e) {
          console.error(e);
        }
      }
    ),
    edit: createAsyncThunk<T, { id: number; data: User }>(
      `${config.entity}/edit`,
      async (request) => {
        try {
          const response = await authAxios.patch(
            config.apiRoute + request.id,
            request.data
          );
          return response.data;
        } catch (e) {
          console.error(e);
        }
      }
    ),

    delete: createAsyncThunk<T, number>(`${config.entity}`, async (id) => {
      const response = await authAxios.delete(config.apiRoute + id);
      return response.data;
    }),

    getById: createAsyncThunk<T, number>(
      `${config.entity}/getById`,
      async (id: number) => {
        try {
          const response = await authAxios.get(config.apiRoute + id);
          return response.data;
        } catch (e) {
          console.error(e);
        }
      }
    ),
  };
};

export default entityApiRest;

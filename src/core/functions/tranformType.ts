export function transformEntityToMantainer<T>(originEntitys: T[]): any[] {
  return originEntitys?.map((entity) => {
    const entitys: any = {} as any;
    for (const key in entity) {
      entitys[key as keyof any] = {
        state: true,
        disable: false,
        name: entity[key],
      };
    }
    return entitys;
  });
}

export function transformEntityToOrigin<
  T extends { [key: string]: { name: any } }
>(entitys: T[]): T[] {
  return entitys.map((entitys) => {
    const entity: T = {} as T;
    for (const key in entitys) {
      if (entitys.hasOwnProperty(key)) {
        entity[key] = entitys[key].name;
      }
    }
    return entity;
  });
}

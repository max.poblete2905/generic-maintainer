export interface FieldConfig {
  name: string;
  label: string;
  type: string;
  defaultValue?: string;
  options?: any[];
}

export interface FormComponentsModel<T> {
  fieldsData: T[];
  onSubmit: (data: T) => void;
  onClosed: () => void;
}

import { Role } from "./Role";
import { MantainerField } from "./MantainerFieds";
export interface User {
  id: number;
  firstName: string;
  lastName: string;
  address: string;
  phoneNumber: string;
  email: string;
  role: Role[];
}

export type UserMantainer = {
  [K in keyof User]: MantainerField;
};

import { MantainerField } from "./MantainerFieds";

export interface Product {
  id: number;
  name: string;
  description: string;
  price: number;
  availableQuantity: number;
  category: string;
}

export type ProductMantainer = {
  [K in keyof Product]: MantainerField;
};

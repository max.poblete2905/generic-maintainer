export type MantainerField = {
  state: boolean;
  disabled: boolean;
  name: any;
  type: string;
  options?: any[];
};

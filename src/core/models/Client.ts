import { User } from "./User";
import { MantainerField } from "./MantainerFieds";

export interface Client extends User {
  dateOfBirth: Date;
  identificationNumber: string;
  accountStatus: boolean;
}

export type ClientMantainer = {
  [K in keyof Client]: MantainerField;
};

import React from "react";
import Grid from "@mui/material/Grid"; // Importa Grid desde Material-UI
import DynamicChart from "./Graphyc";
import { Typography } from "@mui/material";

interface DataPoint {
  name: string;
  value: number;
  color: string; // Campo "color" en el dato
}

const colorPalette = ["#8884d8", "#82ca9d", "#ffc658", "#ff7300"]; // Colores diferentes

const data: DataPoint[] = [
  { name: "A", value: 10, color: colorPalette[0] },
  { name: "B", value: 20, color: colorPalette[1] },
  { name: "C", value: 15, color: colorPalette[3] },
  { name: "D", value: 25, color: colorPalette[4] },
];

const Dashboard: React.FC = () => {
  return (
    <div>
      <Typography>KPIS </Typography>
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <Typography>Gráfico de Barras</Typography>
          <DynamicChart data={data} type="bar" />{" "}
        </Grid>
        <Grid item xs={4}>
          <Typography>Gráfico de Líneas</Typography>
          <DynamicChart data={data} type="line" />{" "}
        </Grid>
        <Grid item xs={4}>
          <Typography>Gráfico de Pastel</Typography>
          <DynamicChart data={data} type="pie" />{" "}
        </Grid>
      </Grid>
    </div>
  );
};

export default Dashboard;

import React, { useState } from "react";
import {
  Container,
  Typography,
  Paper,
  TextField,
  Button,
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  IconButton,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from "@mui/icons-material/Edit";

interface Product {
  id: number;
  name: string;
  quantity: number;
}

const InventoryManager: React.FC = () => {
  const [products, setProducts] = useState<Product[]>([]);
  const [newProduct, setNewProduct] = useState<Product>({
    id: 0,
    name: "",
    quantity: 0,
  });
  const [editProduct, setEditProduct] = useState<Product | null>(null);

  const addProduct = () => {
    if (newProduct.name.trim() === "" || newProduct.quantity <= 0) {
      return;
    }

    setProducts([...products, newProduct]);
    setNewProduct({ id: 0, name: "", quantity: 0 });
  };

  const editProductHandler = (product: Product) => {
    setEditProduct(product);
  };

  const updateProduct = () => {
    if (editProduct) {
      const updatedProducts = products.map((product) =>
        product.id === editProduct.id ? editProduct : product
      );
      setProducts(updatedProducts);
      setEditProduct(null);
    }
  };

  const deleteProduct = (productId: number) => {
    const updatedProducts = products.filter(
      (product) => product.id !== productId
    );
    setProducts(updatedProducts);
  };

  return (
    <Container>
      <Typography variant="h4">Gestión de Inventario</Typography>
      <Paper elevation={3} style={{ padding: "16px", margin: "16px 0" }}>
        <TextField
          label="Nombre del Producto"
          fullWidth
          margin="normal"
          variant="outlined"
          value={newProduct.name}
          onChange={(e) =>
            setNewProduct({ ...newProduct, name: e.target.value })
          }
        />
        <TextField
          label="Cantidad"
          fullWidth
          margin="normal"
          variant="outlined"
          type="number"
          value={newProduct.quantity}
          onChange={(e) =>
            setNewProduct({
              ...newProduct,
              quantity: parseInt(e.target.value, 10),
            })
          }
        />
        <Button
          variant="contained"
          color="primary"
          onClick={addProduct}
          style={{ marginTop: "16px" }}
        >
          Agregar Producto
        </Button>
      </Paper>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Nombre del Producto</TableCell>
              <TableCell>Cantidad</TableCell>
              <TableCell>Acciones</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {products.map((product) => (
              <TableRow key={product.id}>
                <TableCell>{product.id}</TableCell>
                <TableCell>{product.name}</TableCell>
                <TableCell>{product.quantity}</TableCell>
                <TableCell>
                  <IconButton
                    onClick={() => editProductHandler(product)}
                    color="primary"
                  >
                    <EditIcon />
                  </IconButton>
                  <IconButton
                    onClick={() => deleteProduct(product.id)}
                    color="secondary"
                  >
                    <DeleteIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      {editProduct && (
        <Paper elevation={3} style={{ padding: "16px", margin: "16px 0" }}>
          <Typography variant="h6">Editar Producto</Typography>
          <TextField
            label="Nombre del Producto"
            fullWidth
            margin="normal"
            variant="outlined"
            value={editProduct.name}
            onChange={(e) =>
              setEditProduct({ ...editProduct, name: e.target.value })
            }
          />
          <TextField
            label="Cantidad"
            fullWidth
            margin="normal"
            variant="outlined"
            type="number"
            value={editProduct.quantity}
            onChange={(e) =>
              setEditProduct({
                ...editProduct,
                quantity: parseInt(e.target.value, 10),
              })
            }
          />
          <Button
            variant="contained"
            color="primary"
            onClick={updateProduct}
            style={{ marginTop: "16px" }}
          >
            Actualizar Producto
          </Button>
        </Paper>
      )}
    </Container>
  );
};

export default InventoryManager;

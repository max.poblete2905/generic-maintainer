// src/components/Home.tsx
import React from "react";
import { useNavigate } from "react-router";
import ContainerBase from "../components/ContainerBase";

const NotFound: React.FC = () => {
  const navigate = useNavigate();

  const handlerBack = () => {
    navigate("/");
  };

  return (
    <ContainerBase
      content={
        <div>
          <p>Lo sentimos, la página que estás buscando no existe.</p>
          <button onClick={handlerBack}>ir Home</button>
        </div>
      }
      title={"Página no encontrada"}
      subtitle={""}
    />
  );
};

export default NotFound;

import { useForm, Controller } from "react-hook-form";
import { FormComponentsModel } from "../core/models/FieldConfig";
import {
  TextField,
  Button,
  Checkbox,
  FormControlLabel,
  Grid,
  Select,
  MenuItem,
} from "@mui/material";

const FormComponents = ({
  fieldsData,
  onSubmit,
  onClosed,
}: FormComponentsModel<any>) => {
  const { control, handleSubmit, formState } = useForm();
  const { isDirty, isValid } = formState;

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Grid container spacing={2}>
        {fieldsData.map(
          ({ name, defaultValue, label, type, disabled, options }) =>
            // Verificar si disabled es true y omitir el campo si es cierto
            !disabled && (
              <Controller
                key={name}
                name={name}
                control={control}
                defaultValue={defaultValue || ""}
                rules={{ required: true }}
                render={({ field }) => (
                  <Grid item xs={6}>
                    <label htmlFor={name}>{label}</label> {/* Etiqueta */}
                    {type === "check" ? (
                      <FormControlLabel
                        control={
                          <Checkbox
                            {...field}
                            name={name}
                            checked={field.value}
                            onChange={(e) => field.onChange(e.target.checked)}
                            color="primary"
                          />
                        }
                        label=""
                      />
                    ) : type === "options" ? (
                      <Select
                        {...field}
                        value={field.value}
                        label=""
                        variant="outlined"
                        fullWidth
                        error={!!formState.errors[name]}
                        size="small"
                      >
                        {options?.map((option: any) => (
                          <MenuItem key={option?.value} value={option?.value}>
                            {option?.label}
                          </MenuItem>
                        ))}
                      </Select>
                    ) : (
                      <TextField
                        {...field}
                        id={name}
                        label=""
                        variant="outlined"
                        fullWidth
                        type={type}
                        error={!!formState.errors[name]}
                        helperText={
                          formState.errors[name] ? "Campo requerido" : ""
                        }
                        size="small"
                      />
                    )}
                    {/* {JSON.stringify(field)} */}
                  </Grid>
                )}
              />
            )
        )}

        <Grid item xs={12}>
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <Button onClick={onClosed} variant="contained" color="primary">
              cerrar
            </Button>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              disabled={!isDirty || !isValid}
            >
              enviar
            </Button>
          </div>
        </Grid>
      </Grid>
    </form>
  );
};

export default FormComponents;

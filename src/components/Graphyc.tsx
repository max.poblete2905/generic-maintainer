import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  CartesianGrid,
  LineChart,
  Line,
  PieChart,
  Pie,
  Cell, // Importa Cell desde recharts
} from "recharts";

interface DataPoint {
  name: string;
  value: number;
  color: string; // Campo "color" en el dato
}

interface ChartProps {
  data: DataPoint[];
  type?: "bar" | "line" | "pie";
  width?: number;
  height?: number;
}

const DynamicChart: React.FC<ChartProps> = ({
  data,
  type = "bar",
  width = 400,
  height = 300,
}) => {
  let chartComponent;

  switch (type) {
    case "line":
      chartComponent = (
        <LineChart width={width} height={height} data={data}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Line type="monotone" dataKey="value" stroke="#8884d8" />
        </LineChart>
      );
      break;
    case "pie":
      chartComponent = (
        <PieChart width={width} height={height}>
          <Pie
            data={data}
            dataKey="value"
            nameKey="name"
            cx="50%"
            cy="50%"
            outerRadius={60}
            label
          >
            {data.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={entry.color} />
            ))}
          </Pie>
          <Tooltip />
        </PieChart>
      );
      break;
    default:
      chartComponent = (
        <BarChart width={width} height={height} data={data}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="value">
            {data.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={entry.color} />
            ))}
          </Bar>
        </BarChart>
      );
  }

  return chartComponent;
};

export default DynamicChart;

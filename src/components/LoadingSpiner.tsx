export const LoadingSpinner = () => {
  const spinnerStyle = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh", // Establece la altura al 100% de la ventana
  };

  return (
    <div style={spinnerStyle}>
      <div>{""}</div>
    </div>
  );
};

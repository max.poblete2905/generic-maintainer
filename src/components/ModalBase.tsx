import React from "react";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Modal from "@mui/material/Modal";

interface ModalBase {
  content: React.ReactNode;
  title: string;
  subtitle: string;
  setOpen: (open: boolean) => void;
  open: boolean;
  width: string;
}

export default function BasicModal({
  content,
  title,
  subtitle,
  setOpen,
  open,
  width,
}: ModalBase) {
  const handleClose = () => setOpen(false);

  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Card
        sx={{
          position: "absolute" as "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          width: width,
          // bgcolor: "background.paper", // Elimina este fondo
          // border: "2px solid #000", // Elimina este borde
          boxShadow: 24,
          borderRadius: 3,
        }}
      >
        <CardHeader
          title={title}
          subheader={subtitle}
          titleTypographyProps={{ variant: "h6", sx: { color: "white" } }} // Texto blanco
          subheaderTypographyProps={{
            variant: "subtitle1",
            sx: { color: "white" },
          }} // Texto blanco
          sx={{ backgroundColor: "blue" }} // Fondo azul
        />
        <CardContent>{content}</CardContent>
      </Card>
    </Modal>
  );
}

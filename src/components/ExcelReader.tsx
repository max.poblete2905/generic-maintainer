import React, { useState } from "react";
import * as XLSX from "xlsx";
import Dropzone from "react-dropzone";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import TableContainer from "@mui/material/TableContainer";
import Table from "@mui/material/Table";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import DeleteIcon from "@mui/icons-material/Delete";
import { Button } from "@mui/material";

interface ExcelData {
  [key: string]: any;
}

interface ExcelReadModel<T> {
  onSubmit: (data: T) => void;
  onClosed: () => void;
}

const ExcelReader = ({ onSubmit, onClosed }: ExcelReadModel<any>) => {
  const [excelData, setExcelData] = useState<ExcelData[]>([]);

  const handleDrop = (acceptedFiles: File[]) => {
    if (acceptedFiles.length === 0) return;

    const file = acceptedFiles[0];
    const reader = new FileReader();
    reader.onload = (e) => {
      const binaryString = e.target?.result as string;
      const workbook = XLSX.read(binaryString, { type: "binary" });
      const sheetName = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[sheetName];
      const data = XLSX.utils.sheet_to_json(worksheet, { header: 2 });

      const excelObjects = data.map((row: any) => {
        const obj: ExcelData = {};
        Object.keys(row).forEach((key) => {
          obj[key] = row[key];
        });
        return obj;
      });
      setExcelData(excelObjects);
    };

    reader.readAsBinaryString(file);
  };

  const handleDeleteRow = (index: number) => {
    const updatedData = [...excelData];
    updatedData.splice(index, 1);
    setExcelData(updatedData);
  };

  const handleClose = () => {
    setExcelData([]);
    onClosed();
  };

  const dropzoneStyles: React.CSSProperties = {
    border: "2px dashed #ccc",
    borderRadius: "4px",
    padding: "20px",
    textAlign: "center" as const,
    cursor: "pointer",
  };

  const tableContainerStyle: React.CSSProperties = {
    maxHeight: "300px",
    overflowY: "auto" as "auto", // Forzar overflowY a ser "auto" o "hidden"
  };

  const tableCellStyle: React.CSSProperties = {
    fontSize: "14px", // Tamaño de fuente más pequeño
    padding: "8px", // Espaciado más pequeño
  };

  const tableHeaderCellStyle: React.CSSProperties = {
    backgroundColor: "#white",
    color: "black",
    fontWeight: "bold",
    fontSize: "14px", // Tamaño de fuente más pequeño
    padding: "8px", // Espaciado más pequeño
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        {excelData.length === 0 && (
          <Dropzone onDrop={handleDrop}>
            {({ getRootProps, getInputProps }) => (
              <Paper {...getRootProps()} style={dropzoneStyles}>
                <input {...getInputProps()} />
                <p>
                  Arrastra y suelta un archivo Excel aquí o haz clic para
                  seleccionar uno.
                </p>
              </Paper>
            )}
          </Dropzone>
        )}
      </Grid>
      {excelData.length > 0 && (
        <Grid item xs={12}>
          <Button onClick={() => onSubmit(excelData)}>Subir excel</Button>
          <Button onClick={() => setExcelData([])}>Restablecer</Button>
          <Button onClick={handleClose}>Cerrar</Button>
        </Grid>
      )}

      {excelData.length > 0 && (
        <Grid item xs={12}>
          {excelData.length > 0 && (
            <div style={tableContainerStyle}>
              <TableContainer component={Paper}>
                <Table>
                  <TableHead>
                    <TableRow>
                      {Object.keys(excelData[0] || {}).map((header, index) => (
                        <TableCell key={index} style={tableHeaderCellStyle}>
                          {header}
                        </TableCell>
                      ))}
                      <TableCell>{""}</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {excelData.map((row, index) => (
                      <TableRow key={index}>
                        {Object.values(row).map((cell, cellIndex) => (
                          <TableCell key={cellIndex} style={tableCellStyle}>
                            {cell}
                          </TableCell>
                        ))}
                        <TableCell>
                          <DeleteIcon
                            sx={{ cursor: "pointer" }}
                            onClick={() => handleDeleteRow(index)}
                          />
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
          )}
        </Grid>
      )}
    </Grid>
  );
};

export default ExcelReader;

import ContainerBase from "./ContainerBase";
import MantainerEntity from "../modules/mantainers/MantainerEntity";
import { Typography } from "@mui/material";

interface DynamicEntityPageModel<T> {
  entity: T;
  entitys: T[];
  entityName: string;
}

const DynamicEntityPage = ({
  entity,
  entitys,
  entityName,
}: DynamicEntityPageModel<any>) => {
  return (
    <ContainerBase
      content={
        entitys.length > 0 ? (
          <MantainerEntity
            entity={entity}
            entitys={entitys}
            entityName={entityName}
          />
        ) : (
          <Typography>No hay datos, el servidor no responde</Typography>
        )
      }
      title={`Mantenedor ${entityName}s`}
      subtitle={`Módulo para la gestión de ${entityName}s del sistema`}
    />
  );
};

export default DynamicEntityPage;

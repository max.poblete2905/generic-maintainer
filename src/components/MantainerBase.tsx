import Grid from "@mui/material/Grid";
import TransitionWrapper from "./TransitionWrapper";
interface MantainerBaseModel {
  modalFrom: React.ReactNode;
  modalBulkLoad: React.ReactNode;
  actionMenu: React.ReactNode;
  itemSelected: React.ReactNode;
  dataGrid: React.ReactNode;
}

const MantainerBase = ({
  modalFrom,
  actionMenu,
  dataGrid,
  itemSelected,
  modalBulkLoad,
}: MantainerBaseModel) => {
  return (
    <Grid container spacing={3}>
      <TransitionWrapper children={modalFrom} />
      <TransitionWrapper children={modalBulkLoad} />
      <Grid item xs={12}>
        <TransitionWrapper children={actionMenu} />
      </Grid>
      <Grid item xs={8}>
        <TransitionWrapper children={dataGrid} />
      </Grid>
      <Grid item xs={4}>
        <TransitionWrapper children={itemSelected} />
      </Grid>
    </Grid>
  );
};

export default MantainerBase;

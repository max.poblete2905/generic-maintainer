import { Typography, Grid, Paper, Box } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete"; // Importa el icono de eliminar

interface ElementsSelected<T> {
  selectedItems: T[];
}

const ElemenstSelected = <T extends Record<string, any>>({
  selectedItems,
}: ElementsSelected<T>) => {
  const handlerDelete = () => {
    alert(JSON.stringify(selectedItems));
  };

  return (
    <div>
      <Typography variant="h6">Eliminar Elementos seleccionados:</Typography>
      <Box mb={2}>
        <DeleteIcon sx={{ cursor: "pointer" }} onClick={handlerDelete} />
      </Box>
      <Grid container spacing={2}>
        {selectedItems.map((item: T, index: number) => (
          <Grid item xs={8} key={index}>
            <Paper elevation={3} style={{ padding: "8px" }}>
              <Typography>{Object.values(item)[1]}</Typography>
            </Paper>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

export default ElemenstSelected;

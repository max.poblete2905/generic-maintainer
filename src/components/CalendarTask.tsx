import React, { useState } from "react";
import {
  Grid,
  TextField,
  List,
  ListItem,
  ListItemText,
  Checkbox,
  IconButton,
  Select,
  MenuItem,
  FormControl,
  InputLabel,
  Typography,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import formatDistanceToNow from "date-fns/formatDistanceToNow";
import AddIcon from "@mui/icons-material/Add";
import { useToasts } from "react-toast-notifications";
import { es } from "date-fns/locale";

interface Task {
  id: number;
  text: string;
  done: boolean;
  creationDate: Date;
  responsible: string;
}

const TaskCalendar: React.FC = () => {
  const [tasks, setTasks] = useState<Task[]>([
    {
      id: 1,
      text: "Hacer la presentación",
      done: false,
      creationDate: new Date("2023-09-15"),
      responsible: "Usuario 1",
    },
    {
      id: 2,
      text: "Enviar el informe",
      done: true,
      creationDate: new Date("2023-09-14"),
      responsible: "Usuario 2",
    },
    {
      id: 3,
      text: "Reunión de equipo",
      done: false,
      creationDate: new Date("2023-09-16"),
      responsible: "Usuario 3",
    },
  ]);

  const [newTask, setNewTask] = useState<string>("");
  const [newTaskResponsible, setNewTaskResponsible] = useState<string>("");
  const [selectedResponsible, setSelectedResponsible] = useState<string>("");

  const [responsibles, setResponsibles] = useState<string[]>([
    "Usuario 1",
    "Usuario 2",
    "Usuario 3",
    // Agrega más responsables según sea necesario
  ]);

  const { addToast } = useToasts();

  const addTask = () => {
    if (newTask.trim() === "" || newTaskResponsible === "") {
      // Muestra una alerta si alguno de los campos está vacío
      addToast(
        "Por favor, completa todos los campos antes de agregar una tarea",
        {
          appearance: "error",
          autoDismiss: true,
        }
      );
      return;
    }

    const creationDate = new Date(); // Crear la fecha de creación aquí
    const newTaskObj: Task = {
      id: Date.now(),
      text: newTask,
      done: false,
      creationDate, // Usa la fecha de creación creada arriba
      responsible: newTaskResponsible,
    };

    setTasks([...tasks, newTaskObj]);
    setNewTask("");
    setNewTaskResponsible("");
  };

  const deleteTask = (taskId: number) => {
    const taskToDelete = tasks.find((task) => task.id === taskId);

    if (taskToDelete && !taskToDelete.done) {
      // Mostrar una alerta de error si la tarea no está marcada como "done"
      addToast(
        "No puedes eliminar una tarea que no está marcada como completada.",
        {
          appearance: "error",
          autoDismiss: true,
        }
      );
      return;
    }

    const updatedTasks = tasks.filter((task) => task.id !== taskId);
    addToast("Tarea eliminada con éxito", {
      appearance: "success",
      autoDismiss: true,
    });
    setTasks(updatedTasks);
  };

  const toggleTaskDone = (taskId: number) => {
    const updatedTasks = tasks.map((task) => {
      if (task.id === taskId) {
        return { ...task, done: !task.done };
      }
      return task;
    });

    setTasks(updatedTasks);
  };

  const filteredTasks = tasks.filter((task) => {
    if (selectedResponsible === "") {
      return true; // Mostrar todas las tareas si no se ha seleccionado un responsable
    } else {
      return task.responsible === selectedResponsible;
    }
  });

  const formatTimeElapsed = (creationDate: Date): string => {
    const distance = formatDistanceToNow(creationDate, {
      addSuffix: true,
      locale: es, // Establece el idioma en español
    });
    return distance;
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Typography>Gestionar de Tareas</Typography>
      </Grid>
      <Grid item xs={12} md={7}>
        <TextField
          size="small"
          fullWidth
          variant="outlined"
          placeholder="Nueva tarea"
          value={newTask}
          onChange={(e) => setNewTask(e.target.value)}
        />
      </Grid>
      <Grid item xs={12} md={4}>
        <FormControl fullWidth>
          <InputLabel>Responsable</InputLabel>
          <Select
            label="Responsable"
            size="small"
            value={newTaskResponsible}
            onChange={(e) => setNewTaskResponsible(e.target.value as string)}
          >
            {responsibles.map((responsible) => (
              <MenuItem key={responsible} value={responsible}>
                {responsible}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12} md={1}>
        <AddIcon
          sx={{ fontSize: 40, cursor: "pointer" }}
          color="primary"
          onClick={addTask}
        />
      </Grid>
      <Grid item xs={12} md={12}>
        <FormControl fullWidth>
          <InputLabel>Filtrar por responsable</InputLabel>
          <Select
            label="Filtrar por responsable"
            size="small"
            value={selectedResponsible}
            onChange={(e) => setSelectedResponsible(e.target.value as string)}
          >
            <MenuItem value="">Todos</MenuItem>
            {responsibles.map((responsible) => (
              <MenuItem key={responsible} value={responsible}>
                {responsible}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <div style={{ maxHeight: "400px", overflowY: "auto" }}>
          <List>
            {filteredTasks
              .reverse()
              .slice(0, 200)
              .map((task) => {
                const timeElapsed = formatTimeElapsed(task.creationDate);

                return (
                  <ListItem key={task.id}>
                    <Checkbox
                      checked={task.done}
                      onChange={() => toggleTaskDone(task.id)}
                    />
                    <ListItemText
                      primary={task.text}
                      secondary={`Creada ${timeElapsed.replace(
                        "en",
                        "hace"
                      )}, Responsable: ${task.responsible}`}
                      style={{
                        textDecoration: task.done ? "line-through" : "none",
                      }}
                    />
                    <IconButton
                      edge="end"
                      aria-label="Eliminar"
                      onClick={() => deleteTask(task.id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </ListItem>
                );
              })}
          </List>
        </div>
      </Grid>
    </Grid>
  );
};

export default TaskCalendar;

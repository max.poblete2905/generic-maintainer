import React from "react";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import CalendarTask from "./CalendarTask";
import { Typography } from "@mui/material";
import SalesSummary from "./SalesSummary";
import InventoryManager from "./InventoryManager";
const styles = {
  root: {
    padding: "20px",
  },
  section: {
    marginBottom: "20px",
    padding: "10px",
    boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)",
  },
  tableContainer: {
    maxHeight: "300px",
    overflowY: "auto",
  },
  tableHeaderCell: {
    backgroundColor: "#007bff",
    color: "white",
    fontWeight: "bold",
  },
  tableCell: {
    borderBottom: "1px solid #ccc",
  },
};

const tableContainer: React.CSSProperties = {
  maxHeight: "300px",
  overflowY: "auto",
};

const data = [
  { id: 1, name: "Producto A", quantity: 50, price: 10.99 },
  { id: 2, name: "Producto B", quantity: 30, price: 15.49 },
  { id: 3, name: "Producto C", quantity: 20, price: 8.99 },
];

const Home: React.FC = () => {
  return (
    <div style={styles.root}>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={6} md={6}>
          <Paper elevation={5} style={styles.section}>
            <SalesSummary />
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={6}>
          <Paper elevation={5} style={styles.section}>
            <CalendarTask />
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={6}>
          <Paper elevation={5} style={styles.section}>
            <Typography>Gestión de Inventarios</Typography>
            <InventoryManager />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper elevation={5} style={styles.section}>
            <Typography>Notificaciones y Alertas</Typography>
            {/* Componente de notificaciones y alertas */}
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default Home;

import React, { useState, useEffect } from "react";
import {
  Typography,
  Paper,
  Grid,
  Container,
  Box,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";
import { DataGrid, GridColDef } from "@mui/x-data-grid";

interface Sale {
  id: number;
  date: Date;
  amount: number;
}

const columns: GridColDef[] = [
  {
    field: "date",
    headerName: "Fecha",
    width: 200,
    renderCell: (params) => (params.value as Date).toDateString(),
  },
  { field: "amount", headerName: "Cantidad", width: 200, type: "number" },
];

const generateSalesData = () => {
  const salesData: Sale[] = [];

  for (let i = 1; i <= 12; i++) {
    // Genera 10 registros para cada mes
    for (let j = 1; j <= 10; j++) {
      const day = Math.floor(Math.random() * 28) + 1; // Días aleatorios entre 1 y 28
      const amount = Math.floor(Math.random() * 2000) + 100; // Cantidad aleatoria entre 100 y 2100
      const year = 2023;
      const month = i;
      const date = new Date(`${year}-${month}-${day}`);
      const sale: Sale = {
        id: salesData.length + 1,
        date,
        amount,
      };
      salesData.push(sale);
    }
  }

  return salesData;
};

const SalesSummary: React.FC = () => {
  const [sales, setSales] = useState<Sale[]>([]);
  const [filteredSales, setFilteredSales] = useState<Sale[]>([]);
  const [dailySales, setDailySales] = useState<number>(0);
  const [weeklySales, setWeeklySales] = useState<number>(0);
  const [monthlySales, setMonthlySales] = useState<number>(0);
  const now = new Date();
  const initialMonth = now.getMonth(); // Esto obtendrá el número del mes actual (0-11)
  const [selectedMonth, setSelectedMonth] = useState<number | null>(
    initialMonth
  );

  useEffect(() => {
    const mockSalesData: Sale[] = generateSalesData();
    setSales(mockSalesData);
  }, []);

  useEffect(() => {
    // Filtrar las ventas según el mes seleccionado
    if (selectedMonth !== null) {
      const filtered = sales.filter(
        (sale) => sale.date.getMonth() === selectedMonth
      );
      setFilteredSales(filtered);
    }
  }, [selectedMonth, sales]);

  useEffect(() => {
    // Calcular el promedio de ventas diarias
    const dailySalesTotal =
      filteredSales.reduce((total, sale) => total + sale.amount, 0) /
      (filteredSales.length || 1); // Evitar la división por cero
    setDailySales(dailySalesTotal);
  }, [filteredSales]);

  useEffect(() => {
    // Calcular el promedio de ventas semanales
    const weeklySalesTotal =
      filteredSales.reduce((total, sale) => total + sale.amount, 0) /
      (filteredSales.length / 4 || 1); // Promedio semanal (aproximado)
    setWeeklySales(weeklySalesTotal);
  }, [filteredSales]);

  useEffect(() => {
    // Calcular el promedio de ventas mensuales
    const monthlySalesTotal =
      filteredSales.reduce((total, sale) => total + sale.amount, 0) /
      (filteredSales.length / 30 || 1); // Promedio mensual (aproximado)
    setMonthlySales(monthlySalesTotal);
  }, [filteredSales]);

  const mesesDelAnio = [
    { id: 0, month: "Enero" },
    { id: 1, month: "Febrero" },
    { id: 2, month: "Marzo" },
    { id: 3, month: "Abril" },
    { id: 4, month: "Mayo" },
    { id: 5, month: "Junio" },
    { id: 6, month: "Julio" },
    { id: 7, month: "Agosto" },
    { id: 8, month: "Septiembre" },
    { id: 9, month: "Octubre" },
    { id: 10, month: "Noviembre" },
    { id: 11, month: "Diciembre" },
  ];

  return (
    <Container>
      <Grid container spacing={1}>
        <Grid item xs={12} md={12}>
          <FormControl fullWidth>
            <InputLabel>Seleccionar mes</InputLabel>
            <Select
              label="Seleccionar mes"
              value={selectedMonth}
              onChange={(e) => setSelectedMonth(e.target.value as number)}
            >
              {mesesDelAnio.map((item: any) => (
                <MenuItem value={item.id}>{item.month}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={4}>
          <Paper elevation={3}>
            <Box p={2}>
              <Typography variant="h6" align="center">
                Ventas Diarias (Promedio)
              </Typography>
              <Typography variant="h4" align="center">
                ${dailySales.toFixed(2)} {/* Redondear a 2 decimales */}
              </Typography>
            </Box>
          </Paper>
        </Grid>
        <Grid item xs={12} md={4}>
          <Paper elevation={3}>
            <Box p={2}>
              <Typography variant="h6" align="center">
                Ventas Semanales (Promedio)
              </Typography>
              <Typography variant="h4" align="center">
                ${weeklySales.toFixed(2)} {/* Redondear a 2 decimales */}
              </Typography>
            </Box>
          </Paper>
        </Grid>
        <Grid item xs={12} md={4}>
          <Paper elevation={3}>
            <Box p={2}>
              <Typography variant="h6" align="center">
                Ventas Mensuales (Promedio)
              </Typography>
              <Typography variant="h4" align="center">
                ${monthlySales.toFixed(2)} {/* Redondear a 2 decimales */}
              </Typography>
            </Box>
          </Paper>
        </Grid>
      </Grid>
      <Box mt={3}>
        <Paper elevation={3}>
          <DataGrid
            rows={filteredSales}
            columns={columns}
            autoHeight
            hideFooterPagination
          />
        </Paper>
      </Box>
    </Container>
  );
};

export default SalesSummary;

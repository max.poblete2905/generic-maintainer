import { useState, useEffect, ReactNode } from "react";
interface TransitionWrapperProps {
  children: ReactNode;
  transitionType?: string;
  duration?: number;
  delay?: number;
}

const TransitionWrapper = ({
  children,
  transitionType,
  duration,
  delay,
}: TransitionWrapperProps) => {
  const [show, setShow] = useState(false);

  useEffect(() => {
    // Usar un retardo opcional antes de aplicar la transición
    const timer = setTimeout(() => {
      setShow(true);
    }, delay || 0);

    return () => clearTimeout(timer);
  }, [delay]);

  const transitionStyles = {
    transition: `${transitionType || "opacity"} ${
      duration || 0.4
    }s ease-in-out`,
    opacity: show ? 1 : 0,
  };

  return <div style={transitionStyles}>{children}</div>;
};

export default TransitionWrapper;

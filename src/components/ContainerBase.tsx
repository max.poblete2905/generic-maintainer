import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";

interface ContainerBaseModel {
  content: React.ReactNode;
  title: string;
  action?: React.ReactNode;
  numberColumns?: number;
  subtitle: string;
}

const ContainerBase = ({
  content,
  action = undefined,
  title,
  numberColumns = 12,
  subtitle,
}: ContainerBaseModel) => {
  return (
    <Box
      sx={{
        width: "100%", // Ajusta el ancho al 100% de la pantalla
        minWidth: "100%", // Puedes ajustar el maxWidth según tus necesidades
        margin: "0 auto", // Centra el componente horizontalmente
        marginTop: 1,
        marginBottom: 1,
        padding: 1,
      }}
    >
      <Grid container spacing={3}>
        <Grid item xs={4}>
          <Typography variant="h5">{title}</Typography>
          {subtitle && ( // Renderizamos el subtítulo si está presente
            <Typography variant="subtitle1">{subtitle}</Typography>
          )}
        </Grid>
        {action && (
          <Grid item xs={2}>
            {action}
          </Grid>
        )}
        <Grid item xs={numberColumns}>
          {content}
        </Grid>
      </Grid>
    </Box>
  );
};

export default ContainerBase;

import PersonIcon from "@mui/icons-material/Person";
import StackedBarChartIcon from "@mui/icons-material/StackedBarChart";
import ContactsIcon from "@mui/icons-material/Contacts";
import AddHomeIcon from "@mui/icons-material/AddHome";
import InventoryIcon from "@mui/icons-material/Inventory";

export const modules = [
  {
    id: 0,
    name: "Home",
    description: "Main control panel",
    icon: <AddHomeIcon />,
    path: "/home",
  },
  {
    id: 1,
    name: "Dashboard",
    description: "Main control panel",
    icon: <StackedBarChartIcon />,
    path: "/dashboard",
  },
  {
    id: 2,
    name: "Users",
    description: "User and employee management",
    icon: <PersonIcon />,
    path: "/users",
  },
  {
    id: 3,
    name: "Clients",
    description: "Client and contact management",
    icon: <ContactsIcon />,
    path: "/clients",
  },
  {
    id: 3,
    name: "Productos",
    description: "Client and contact management",
    icon: <InventoryIcon />,
    path: "/products",
  },
];

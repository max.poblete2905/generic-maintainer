import * as React from "react";
import { DataGrid, GridColDef, GridRowId } from "@mui/x-data-grid";
import { IconButton } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";

interface DataGridComponentsInterface<T> {
  rows: T[];
  columns: GridColDef[];
  selectedItems: T[];
  setSelectedItems: React.Dispatch<React.SetStateAction<T[]>>;
  onEditClick: (item: T) => void;
}

const DataGridComponents = <T extends any>({
  rows,
  columns,
  selectedItems,
  setSelectedItems,
  onEditClick,
}: DataGridComponentsInterface<T>) => {
  const handleSelectionChange = (selectionModel: GridRowId[]) => {
    console.log(selectionModel);
    const selectedRows: T[] = selectionModel.map((id) => {
      const row = rows.find(
        (item: any) => item?.id === parseInt(id?.toString())
      );
      if (row) {
        return row;
      }
      return {} as T; // Opcional: Si el elemento no se encuentra, puedes devolver un objeto vacío.
    });

    setSelectedItems(selectedRows);
  };

  const handleRowClick = (params: any) => {
    const clickedRowId = params.row.id;
    const isRowSelected = selectedItems.some(
      (item: any) => item.id === clickedRowId
    );

    if (isRowSelected) {
      // Si la fila ya está seleccionada, deselecciónala
      setSelectedItems((prevSelectedItems) =>
        prevSelectedItems.filter((item: any) => item.id !== clickedRowId)
      );
    } else {
      // Si la fila no está seleccionada, selecciónala
      const clickedRow = rows.find((item: any) => item.id === clickedRowId);
      if (clickedRow) {
        setSelectedItems((prevSelectedItems) => [
          ...prevSelectedItems,
          clickedRow,
        ]);
      }
    }
  };

  const handleEditClick = (params: any) => {
    // Verificamos si el clic provino del botón de edición
    if (params.field === "action") {
      const item = rows.find((row: any) => row.id === params.row.id);
      if (item) {
        onEditClick(item); // Llamamos al callback con el elemento seleccionado
      }
    }
  };

  // Estilos personalizados para desactivar la selección de fila y columna
  const noSelectStyles = {
    userSelect: "none",
    MozUserSelect: "none",
    WebkitUserSelect: "none",
    msUserSelect: "none",
  };

  // Estilos personalizados para desactivar el borde de enfoque al hacer clic
  const noFocusStyles = {
    outline: "none !important",
  };

  // Agregamos una columna personalizada para el botón "Editar"
  const customColumns: GridColDef[] = [
    ...columns,
    {
      field: "action",
      headerName: "Acción",
      width: 80,
      sortable: false,
      renderCell: (params) => {
        return (
          <IconButton
            aria-label="Editar"
            sx={{
              ...noSelectStyles,
              ...noFocusStyles,
            }}
            onClick={(e) => {
              e.stopPropagation(); // Evita la propagación del evento de selección
              handleEditClick(params);
            }}
          >
            <EditIcon />
          </IconButton>
        );
      },
    },
  ];

  return (
    <DataGrid
      rows={rows}
      columns={customColumns.filter((col: any) => !col.disabled)}
      sx={{
        "& .MuiDataGrid-cell": {
          ...noSelectStyles,
          ...noFocusStyles,
        },
        "& .MuiDataGrid-row": {
          ...noSelectStyles,
          ...noFocusStyles,
        },
      }}
      initialState={{
        pagination: {
          paginationModel: { page: 0, pageSize: 5 },
        },
      }}
      pageSizeOptions={[5, 10]}
      checkboxSelection
      rowSelectionModel={selectedItems.map((item: any) => item.id)}
      onRowSelectionModelChange={handleSelectionChange}
      onRowClick={handleRowClick}
    />
  );
};

export default DataGridComponents;

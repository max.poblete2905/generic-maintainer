import React from "react";
import { Route, Routes } from "react-router-dom";

const Home = React.lazy(() => import("../components/Home"));
const Dashboards = React.lazy(() => import("../components/Dashboard"));
const NotFound = React.lazy(() => import("../components/NotFound"));
const UsersPage = React.lazy(() => import("../pages/users/UsersPage"));
const ClientPage = React.lazy(() => import("../pages/clients/ClientsPage"));
const ProductPage = React.lazy(() => import("../pages/products/ProductsPage"));

const AppRouter: React.FC = () => {
  return (
    <Routes>
      <Route
        path=""
        element={
          <React.Suspense fallback={<div>Loading...</div>}>
            <Home />
          </React.Suspense>
        }
      />
      <Route
        path="home"
        element={
          <React.Suspense fallback={<div>Loading...</div>}>
            <Home />
          </React.Suspense>
        }
      />
      <Route
        path="dashboard"
        element={
          <React.Suspense fallback={<div>Loading...</div>}>
            <Dashboards />
          </React.Suspense>
        }
      />
      <Route
        path="Clients"
        element={
          <React.Suspense fallback={<div>Loading...</div>}>
            <ClientPage />
          </React.Suspense>
        }
      />
      <Route
        path="users"
        element={
          <React.Suspense fallback={<div>Loading...</div>}>
            <UsersPage />
          </React.Suspense>
        }
      />
      <Route
        path="products"
        element={
          <React.Suspense fallback={<div>Loading...</div>}>
            <ProductPage />
          </React.Suspense>
        }
      />

      <Route
        path="*"
        element={
          <React.Suspense fallback={<div>Loading...</div>}>
            <NotFound />
          </React.Suspense>
        }
      />
    </Routes>
  );
};

export default AppRouter;

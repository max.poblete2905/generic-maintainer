import { useEffect } from "react";
import { useAppSelector, useAppDispatch } from "../../redux";
import clientApi from "../../redux/clients/clientApi";
import DynamicEntityPage from "../../components/DynamicEntityPage";
import { LoadingSpinner } from "../../components/LoadingSpiner";

const ClientsPage = () => {
  const dispatch = useAppDispatch();
  const { clients, loading } = useAppSelector((state) => state.clientsList);
  const { client, entity } = useAppSelector((state) => state.clientSlice);

  useEffect(() => {
    dispatch(clientApi.list());
  }, [dispatch]);

  return (
    <div>
      {loading ? (
        <LoadingSpinner />
      ) : (
        <DynamicEntityPage
          entity={client}
          entitys={clients}
          entityName={entity}
        />
      )}
    </div>
  );
};

export default ClientsPage;

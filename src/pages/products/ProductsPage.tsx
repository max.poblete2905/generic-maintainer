import { useEffect } from "react";
import { useAppSelector, useAppDispatch } from "../../redux";
import productApi from "../../redux/products/productApi";
import DynamicEntityPage from "../../components/DynamicEntityPage";
import { LoadingSpinner } from "../../components/LoadingSpiner";

const ProductsPage = () => {
  const dispatch = useAppDispatch();
  const { products, loading, entity } = useAppSelector(
    (state) => state.productsListSlice
  );
  const { product } = useAppSelector((state) => state.productSlice);

  useEffect(() => {
    dispatch(productApi.list());
  }, [dispatch]);

  return (
    <div>
      {loading ? (
        <LoadingSpinner />
      ) : (
        <DynamicEntityPage
          entity={product}
          entitys={products}
          entityName={entity}
        />
      )}
    </div>
  );
};

export default ProductsPage;

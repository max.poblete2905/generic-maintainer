import { useEffect } from "react";
import { useAppSelector, useAppDispatch } from "../../redux";
import DynamicEntityPage from "../../components/DynamicEntityPage";
import userApi from "../../redux/users/userApi";
import { LoadingSpinner } from "../../components/LoadingSpiner";

const UsersPage = () => {
  const dispatch = useAppDispatch();
  const { users, loading, entity } = useAppSelector((state) => state.usersList);
  const { user } = useAppSelector((state) => state.userSlice);

  useEffect(() => {
    dispatch(userApi.list());
  }, [dispatch]);

  return (
    <div>
      {loading ? (
        <LoadingSpinner />
      ) : (
        <DynamicEntityPage entity={user} entitys={users} entityName={entity} />
      )}
    </div>
  );
};

export default UsersPage;

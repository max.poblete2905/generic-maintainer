import React, { useEffect, useState } from "react";
import { GridColDef } from "@mui/x-data-grid";
import { Button } from "@mui/material";
import { FieldConfig, User } from "../../core/models";
import { transformEntityToOrigin } from "../../core/functions/tranformType";
import ModalBase from "../../components/ModalBase";
import ElementsSelected from "../../components/ElementsSelected";
import DataGridComponents from "../../components/DataGrid";
import FormComponents from "../../components/FormComponents";
import PersonAddIcon from "@mui/icons-material/PersonAdd";
import MantainerBase from "../../components/MantainerBase";
import SettingsIcon from "@mui/icons-material/Settings";
import ExcelReader from "../../components/ExcelReader";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";

interface MantainersModel<T> {
  entity: T;
  entitys: T[];
  entityName: string;
}

type Entity = Record<string, any>;
type FunctionName = "actionCreate" | "actionSetting" | "actionReadExcel";

type MantainerMenu = {
  [key: string]: {
    name: FunctionName;
    icon: React.ReactElement;
  };
};

const mantainerMenu: MantainerMenu = {
  actionCreate: {
    icon: <PersonAddIcon />,
    name: "actionCreate",
  },
  actionSetting: {
    icon: <SettingsIcon />,
    name: "actionSetting",
  },
  actionReadExcel: {
    icon: <CloudUploadIcon />,
    name: "actionReadExcel",
  },
};

const MantainerEntity = <T extends Entity>({
  entity,
  entitys,
  entityName,
}: MantainersModel<T>) => {
  const [form, setForm] = useState({ title: "", subtittle: "" });
  const [selectedItems, setSelectedItems] = useState<T[]>([]);
  const [openModal, setOpenModal] = useState(false);
  const [openModalBulkLoad, setOpenModalBulkLoad] = useState(false);

  const [fields, setFields] = useState<FieldConfig[]>([]);
  const [columns, setColumns] = useState<GridColDef[]>([]);

  useEffect(() => {
    const keys = Object.keys(entity);
    setFields(
      keys.map((key) => ({
        name: key,
        label: key.charAt(0).toUpperCase() + key.slice(1),
        defaultValue: entity[key].name,
        type: entity[key].type,
        disabled: entity[key].disabled,
        options: entity[key].options,
      }))
    );

    setColumns(
      keys.map((key) => ({
        field: key,
        headerName: key.charAt(0).toUpperCase() + key.slice(1),
        width: 100,
        disabled: entity[key].disabled,
      }))
    );
  }, [entity]);

  const actionsEdit = (data: T) => {
    console.log(data);
    const updatedFields = fields.map((field, index) => ({
      ...field,
      defaultValue: Object.values(data)[index],
    }));
    setFields(updatedFields);
    setForm({
      ...form,
      title: `Actualizar ${entityName}`,
      subtittle: `Formulario para actualizar un ${entityName}`,
    });
    setOpenModal(true);
  };

  const handlerClosed = () => {
    const cleanFields = fields.map((field) => ({
      ...field,
      defaultValue: "",
    }));
    setOpenModal(false);
    setFields(cleanFields);
  };

  const handlerClosedModalBulkLoad = () => {
    setOpenModalBulkLoad(false);
  };

  const hadlerSubmit = (data: User) => {
    alert(JSON.stringify(data));
  };

  const actionsMenu = {
    actionSetting: () => {
      alert("setting");
    },
    actionCreate: () => {
      setForm({
        ...form,
        title: `Crear ${entityName}`,
        subtittle: `Formulario para crear un nuevo ${entityName}`,
      });
      setFields(fields);
      setOpenModal(true);
    },

    actionReadExcel: () => {
      setOpenModalBulkLoad(true);
    },
  };

  return (
    <>
      <MantainerBase
        modalBulkLoad={
          <ModalBase
            content={
              <ExcelReader
                onSubmit={hadlerSubmit}
                onClosed={handlerClosedModalBulkLoad}
              />
            }
            title={"Carga Masiva "}
            subtitle={"carda masiva de datos desde archivo excel "}
            setOpen={setOpenModalBulkLoad}
            open={openModalBulkLoad}
            width={"80%"}
          />
        }
        modalFrom={
          <ModalBase
            content={
              <FormComponents
                fieldsData={fields}
                onSubmit={hadlerSubmit}
                onClosed={handlerClosed}
              />
            }
            subtitle={form.subtittle}
            title={form.title}
            setOpen={setOpenModal}
            open={openModal}
            width={"50%"}
          />
        }
        actionMenu={Object.keys(mantainerMenu).map((key: string) => {
          return (
            <Button
              key={key}
              onClick={actionsMenu[mantainerMenu[key].name]}
              startIcon={mantainerMenu[key].icon}
            />
          );
        })}
        dataGrid={
          <DataGridComponents
            selectedItems={selectedItems}
            setSelectedItems={setSelectedItems}
            rows={transformEntityToOrigin(entitys)}
            columns={columns}
            onEditClick={actionsEdit}
          />
        }
        itemSelected={
          selectedItems.length > 0 && (
            <ElementsSelected selectedItems={selectedItems} />
          )
        }
      />
    </>
  );
};

export default MantainerEntity;

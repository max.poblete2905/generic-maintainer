import { createSlice, SerializedError } from "@reduxjs/toolkit";
import { ClientMantainer } from "../../core/models";

// Define el estado inicial del cliente
const client: ClientMantainer = {
  id: {
    state: false,
    disabled: true,
    name: undefined,
    type: "",
  },
  firstName: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  lastName: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  address: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  phoneNumber: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  email: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  role: {
    state: false,
    disabled: false,
    name: undefined,
    type: "options",
    options: [
      { label: "administrador", value: "administrador" },
      { label: "gestor", value: "gestor" },
      { label: "visita", value: "visita" },
    ],
  },
  dateOfBirth: {
    state: false,
    disabled: false,
    name: undefined,
    type: "date",
  },
  identificationNumber: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  accountStatus: {
    state: false,
    disabled: false,
    name: undefined,
    type: "check",
  },
};

export interface ClientListSliceState {
  client: ClientMantainer;
  loading: boolean;
  error: null | SerializedError;
  entity: string;
}

const initialState: ClientListSliceState = {
  client: client,
  loading: false,
  error: null,
  entity: "cliente",
};

// Crea un slice de Redux para el cliente
export const clientSlice = createSlice({
  name: "client",
  initialState,
  reducers: {
    // Define acciones para actualizar propiedades específicas del cliente si es necesario
    // Agrega más acciones según sea necesario
  },
});

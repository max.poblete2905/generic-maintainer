import { ClientMantainer } from "../../core/models";
import clientApi from "./clientApi";
import { createSlice, SerializedError } from "@reduxjs/toolkit";
import { transformEntityToMantainer } from "../../core/functions/tranformType";

export interface ClientListSliceState {
  clients: ClientMantainer[];
  loading: boolean;
  error: null | SerializedError;
}

const initialState: ClientListSliceState = {
  clients: [],
  loading: false,
  error: null,
};

export const clientsListSlice = createSlice({
  name: "clients",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(clientApi.list.pending, (state) => {
      state.clients = [];
      state.loading = true;
    });
    builder.addCase(clientApi.list.fulfilled, (state, action) => {
      state.clients = transformEntityToMantainer(action.payload);
      state.loading = false;
    });
    builder.addCase(clientApi.list.rejected, (state, action) => {
      state.clients = [];
      state.loading = false;
      state.error = action.error;
    });
  },
});

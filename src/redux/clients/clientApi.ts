import entityApiRest from "../../core/axios/entityApiRest";
import { Client } from "../../core/models";

const clientApi = entityApiRest<Client>({
  entity: "clients",
  apiRoute: "clients/",
});

export default clientApi;

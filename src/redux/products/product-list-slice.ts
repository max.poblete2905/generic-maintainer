import { ProductMantainer } from "../../core/models";
import productApi from "./productApi";
import { createSlice, SerializedError } from "@reduxjs/toolkit";
import { transformEntityToMantainer } from "../../core/functions/tranformType";

export interface ProductListSliceState {
  products: ProductMantainer[];
  loading: boolean;
  error: null | SerializedError;
  entity: string;
}

const initialState: ProductListSliceState = {
  products: [],
  loading: false,
  error: null,
  entity: "producto",
};

export const productsListSlice = createSlice({
  name: "products",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(productApi.list.pending, (state) => {
      state.products = [];
      state.loading = true;
    });
    builder.addCase(productApi.list.fulfilled, (state, action) => {
      const productFormat = transformEntityToMantainer(action.payload);
      state.products = productFormat;
      state.loading = false;
    });
    builder.addCase(productApi.list.rejected, (state, action) => {
      state.products = [];
      state.loading = false;
      state.error = action.error;
    });
  },
});

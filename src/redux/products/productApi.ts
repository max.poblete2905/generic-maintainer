import entityApiRest from "../../core/axios/entityApiRest";
import { Product } from "../../core/models";

const productApi = entityApiRest<Product>({
  entity: "products",
  apiRoute: "products/",
});

export default productApi;

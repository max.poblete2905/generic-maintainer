import { createSlice, SerializedError } from "@reduxjs/toolkit";
import { ProductMantainer } from "../../core/models";

const product: ProductMantainer = {
  id: {
    state: false,
    disabled: true,
    name: undefined,
    type: "",
  },
  name: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  description: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  price: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  availableQuantity: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  category: {
    state: false,
    disabled: false,
    name: undefined,
    type: "options",
    options: [
      { label: "Electrónica", value: "electronica" },
      { label: "Ropa", value: "ropa" },
      { label: "Hogar", value: "hogar" },
      { label: "Juguetes", value: "juguetes" },
      { label: "Alimentos", value: "alimentos" },
      { label: "Automóviles", value: "automoviles" },
    ],
  },
};

export interface ProductListSliceState {
  product: ProductMantainer;
  loading: boolean;
  error: null | SerializedError;
}

const initialState: ProductListSliceState = {
  product: product,
  loading: false,
  error: null,
};

export const productSlice = createSlice({
  name: "producto",
  initialState,
  reducers: {
    // Agrega más acciones según sea necesario
  },
});

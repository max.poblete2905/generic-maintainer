import { UserMantainer } from "../../core/models";
import userApi from "./userApi";
import { createSlice, SerializedError } from "@reduxjs/toolkit";
import { transformEntityToMantainer } from "../../core/functions/tranformType";
export interface UserListSliceState {
  users: UserMantainer[];
  loading: boolean;
  error: null | SerializedError;
  entity: string;
}

const initialState: UserListSliceState = {
  users: [],
  loading: false,
  error: null,
  entity: "usuario",
};

export const usersListSlice = createSlice({
  name: "users",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(userApi.list.pending, (state) => {
      state.users = [];
      state.loading = true;
    });
    builder.addCase(userApi.list.fulfilled, (state, action) => {
      console.log();
      const userFormat = transformEntityToMantainer(action.payload);
      state.users = userFormat;
      state.loading = false;
    });
    builder.addCase(userApi.list.rejected, (state, action) => {
      state.users = [];
      state.loading = false;
      state.error = action.error;
    });
  },
});

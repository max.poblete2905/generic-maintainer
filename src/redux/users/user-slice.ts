import { createSlice, PayloadAction, SerializedError } from "@reduxjs/toolkit";
import { UserMantainer } from "../../core/models";

const user: UserMantainer = {
  id: {
    state: false,
    disabled: true,
    name: undefined,
    type: "",
  },
  firstName: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  lastName: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  address: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  phoneNumber: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  email: {
    state: false,
    disabled: false,
    name: undefined,
    type: "",
  },
  role: {
    state: false,
    disabled: false,
    name: undefined,
    type: "options",
    options: [
      { label: "administrador", value: "administrador" },
      { label: "gestor", value: "gestor" },
      { label: "visita", value: "visita" },
    ],
  },
};

export interface UserListSliceState {
  user: UserMantainer;
  loading: boolean;
  error: null | SerializedError;
}

const initialState: UserListSliceState = {
  user: user,
  loading: false,
  error: null,
};

// Crea un slice de Redux para el cliente
export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    // Define acciones para actualizar propiedades específicas del cliente si es necesario
    setFirstName: (state, action: PayloadAction<string>) => {
      state.user.firstName.name = action.payload;
    },
    setLastName: (state, action: PayloadAction<string>) => {
      state.user.firstName.name = action.payload;
    },
    // Agrega más acciones según sea necesario
  },
});

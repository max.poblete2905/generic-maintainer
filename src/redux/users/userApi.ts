import entityApiRest from "../../core/axios/entityApiRest";
import { User } from "../../core/models";

const userApi = entityApiRest<User>({
  entity: "users",
  apiRoute: "users/",
});

export default userApi;

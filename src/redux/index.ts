import { clientsListSlice } from "./clients/clients-list-slice";
import { usersListSlice } from "./users/user-list-slice";
import { userSlice } from "./users/user-slice";
import { productSlice } from "./products/product-slice";
import { productsListSlice } from "./products/product-list-slice";

import { clientSlice } from "./clients/client-slice";
import { combineReducers, configureStore } from "@reduxjs/toolkit";
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";

const reducers = combineReducers({
  usersList: usersListSlice.reducer,
  clientsList: clientsListSlice.reducer,
  clientSlice: clientSlice.reducer,
  userSlice: userSlice.reducer,
  productSlice: productSlice.reducer,
  productsListSlice: productsListSlice.reducer,
});

export const store = configureStore({
  reducer: reducers,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }),
});

export type RootState = ReturnType<typeof reducers>;

export type AppDispatch = typeof store.dispatch;

export const useAppDispatch: () => AppDispatch = useDispatch;
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
